const mix = require('laravel-mix');

mix
    .disableSuccessNotifications()
    .sass('./resources/scss/app.scss', 'dist')
    .sass('./resources/scss/video.scss', 'dist')
    .sass('./resources/scss/admin/app.scss', 'dist/admin')
    .js('./resources/js/video.js', 'dist')
    .js('./resources/js/app.js', 'dist')
    .js('./resources/js/admin/app.js', 'dist/admin')
    .vue()
    .sourceMaps()
    .setPublicPath('public')