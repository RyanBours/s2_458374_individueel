import path, { join } from 'path'
import { createStream } from 'rotating-file-stream'

import dotenv from 'dotenv'
dotenv.config({ path: __dirname + '/.env' })
process.env.__root = path.resolve(__dirname)

import { I18n } from 'i18n'

import express from 'express'
import { NextFunction, Request, Response } from 'express';
import session from 'express-session'

import csrf from 'csurf'
import morgan from 'morgan'

import * as db from './db'
import { PassportLocalModel } from 'mongoose'
import { IUser, User } from './models/user'

import passport from 'passport'

import router from './routing/routes'
import { default as routerAdmin } from './routing/routesAdmin'
import v1 from './api/v1'

const accessLogStream = createStream('access.log', {
    size: '10M',
    interval: '1d',
    compress: 'gzip',
    path: join(__dirname, 'logs')
})

const MongoStore = require('connect-mongo').default;

const PORT = process.env.PORT || 8080
const app: express.Application = express();

const i18n = new I18n()
i18n.configure({
    locales: ['en', 'nl'],
    directory: join(__dirname, 'locales'),
    queryParameter: 'lang',
})

db.init();


type _User = IUser;
declare global {
    namespace Express {
        interface User extends PassportLocalModel<_User> {
        }
    }
}

passport.use(User.createStrategy())
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())

app.set('view engine', 'pug')
    .use(express.static(join(__dirname, 'public')))
    .use(morgan('combined', { stream: accessLogStream }))

    .use(express.json())
    .use(express.urlencoded({
        extended: true
    }))

    .use('/api/v1', v1)

    .use(session({
        secret: process.env.APP_KEY || 'secret',
        store: MongoStore.create({
            mongoUrl: process.env.DB_URL! + process.env.DB_NAME!,
        }),
        resave: false,
        saveUninitialized: true,
    }))

    .use(passport.initialize())
    .use(passport.session())

    .use(csrf())
    .use(i18n.init)
    .use('/admin', routerAdmin)
    .use(router)

    // 404
    .use((req: Request, res: Response, next) => {
        res.sendStatus(404);
    })

    // error handler
    .use((err: any, req: Request, res: Response, next: NextFunction) => {
        console.log(err)
        res.sendStatus(err.status || 500);
    })

    .listen(
        PORT,
        () => console.log(`running on port ${PORT}`)
    )

