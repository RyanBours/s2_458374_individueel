// import moment from 'moment'
import _ from 'lodash'
import { NextFunction, Request, Response } from 'express';

export default function attachPugUtil(req: Request, res: Response, next: NextFunction): void {
    // res.locals.moment = moment
    res.locals._ = _
    next()
}