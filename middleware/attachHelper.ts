import { NextFunction, Request, Response } from 'express';

export default function attachCsrfToken(req: Request, res: Response, next: NextFunction): void {
    res.locals.isAuthenticated = req.user ? true : false
    if (req.user) res.locals.user = req.user
    next()
}