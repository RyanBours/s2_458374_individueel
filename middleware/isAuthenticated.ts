import { NextFunction, Request, Response } from 'express';

export default function isAuthenticated(req: Request, res: Response, next: NextFunction): void {
    if (!req.user) res.redirect('/login')
    next()
}