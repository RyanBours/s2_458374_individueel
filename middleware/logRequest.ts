import { NextFunction, Request, Response } from 'express';

interface IConfig {
    filterMethods?: Array<string>
}

export default function logRequest(config: IConfig = {}) {
    return (req: Request, res: Response, next: NextFunction) => {
        const isProduction = process.env.NODE_ENV === 'production';
        if (!isProduction) {
            for (let method of config!.filterMethods || ['GET', 'POST', 'PUT', 'DELETE']) {
                if (req.method == method) {
                    console.log('%s %s %s', req.method, req.url, req.path)
                    console.log("body", req.body)
                    console.log("query", req.query)
                    console.log("headers", req.headers)
                    console.log("status", res.statusCode)
                }
            }
        }
        next()
    }
}