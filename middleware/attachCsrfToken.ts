import { NextFunction, Request, Response } from 'express';
// TODO: attach csrf to header via js
export default function attachCsrfToken(req: Request, res: Response, next: NextFunction):void {
    res.locals.csrfTokenFunction = req.csrfToken
    next()
}