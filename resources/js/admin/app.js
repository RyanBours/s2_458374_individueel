import tippy from 'tippy.js';
try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap')
    require('../../../node_modules/startbootstrap-sb-admin-2/js/sb-admin-2.min')
} catch (e) {}

import Vue from 'vue'

// Vue.component('helloworld', require('../../components/helloworld.vue').default)

new Vue({
    el: "#app",
});

tippy('#tooltip', {
    allowHTML: true,
    interactive: true,
})

$('select.content-toggle').on('change', () => {
    $('select.content-toggle [href]').each((i, e) => {
        let target = $(e).attr('href')
        $(target).hide()
    })
    let target = $('select.content-toggle option:selected').attr('href')
    $(target).show()
})