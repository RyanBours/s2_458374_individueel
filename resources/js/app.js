// shared libs and functions
import _ from 'lodash'

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

// import moment from 'moment'
// window.moment = moment;