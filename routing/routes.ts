import express from 'express';
import passport from 'passport'

// middleware
import attachCsrfToken from '../middleware/attachCsrfToken'
// import attachPaginationHelper from '../middleware/AttachPaginationHelper'
import isAuthenticated from '../middleware/isAuthenticated'
import attachPugUtil from '../middleware/attachPugUtil'
import attachHelper from '../middleware/attachHelper'
import logRequest from '../middleware/logRequest'

// controllers
import * as Index from '../controller/ControllerIndex'
import * as Auth from '../controller/ControllerAuthentication'
import * as Dashboard from '../controller/ControllerDashboard'
import * as Watch from '../controller/ControllerWatch'

const router = express.Router();

router.use(logRequest())
router.use(attachCsrfToken)
// router.use(attachPaginationHelper)
router.use(attachPugUtil)
router.use(attachHelper)

// Auth routes
router.get('/login', Auth.login)
router.post('/login', passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login' }))
router.get('/register', Auth.register)
router.post('/register', Auth.registerPost)
router.get('/logout', Auth.logout)

// Routes
router.get(['/', '/index'], Index.index)

router.use(isAuthenticated)
router.get(['/dashboard'], Dashboard.index)

router.get(['/watch'], (req, res) => res.redirect('dashboard'))
router.get(['/watch/:id'], Watch.show)

export default router;