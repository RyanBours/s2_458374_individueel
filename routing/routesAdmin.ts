import express from 'express';

import * as Dashboard from '../controller/admin/ControllerDashboard'
import * as Serie from '../controller/admin/ControllerSerie'
import * as Season from '../controller/admin/ControllerSeason'
import * as Episode from '../controller/admin/ControllerEpisode'
import * as AltSource from '../controller/admin/ControllerAltSource'

// middleware
import logRequest from '../middleware/logRequest'
import attachCsrfToken from '../middleware/attachCsrfToken'
// import attachPugUtil from '../middleware/attachPugUtil'
// import attachHelper from '../middleware/attachHelper'

const admin = express.Router();

admin.use(logRequest({filterMethods:['POST']}))
admin.use(attachCsrfToken)
// admin.use(attachPugUtil)
// admin.use(attachHelper)

admin.get(['/', '/index', '/dashboard'], Dashboard.index)

admin.get('/serie/create', Serie.create)
admin.post('/serie/create', Serie.post)
admin.get('/serie/show/:id', Serie.show)
admin.get(['/series/', '/serie/index'], Serie.index)

admin.get('/season/create', Season.create)
admin.post('/season/create', Season.post)
// admin.get('/seasons', Season.index)

admin.get('/episode/create', Episode.create)
admin.post('/episode/create', Episode.post)
// admin.get('/episodes', Episode.index)

admin.get('/altsource/create', AltSource.create)
admin.post('/altsource/create', AltSource.post)

export default admin;