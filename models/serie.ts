import { model, Schema, Model, Document, Types } from 'mongoose'

export interface ISerie extends Document {
    title: string;
    description: string;
    publish: boolean;
}

const SerieSchema: Schema = new Schema({
    title: { type: String, required: true },
    description: { type: String, default: 'No synopsis given.' },
    publish: { type: Boolean, default: false },
}, {
    timestamps: {}
})

SerieSchema.virtual('seasons', {
    ref: 'Season',
    localField: '_id',
    foreignField: 'serie'
})

// SerieSchema.pre('save', function (next) {
//     next()
// })

export const Serie: Model<ISerie> = model('Serie', SerieSchema)