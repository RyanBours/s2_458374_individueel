import { model, Schema, Model, Document, Types } from 'mongoose'

export interface ISeason extends Document {
    serie: Types.ObjectId;
    title: string;
    description: string;
    sort_order: number;
}

const SeasonSchema: Schema = new Schema({
    serie: { type: Types.ObjectId, ref: 'Serie', required: true },
    title: { type: String, required: true },
    description: { type: String },
    sort_order: { type: Number, required: true, default: 0 },
}, {
    timestamps: {}
})

SeasonSchema.virtual('episodes', {
    ref: 'Episode',
    localField: '_id',
    foreignField: 'season'
})

// SeasonSchema.pre('save', function (next) {
//     next()
// })

export const Season: Model<ISeason> = model('Season', SeasonSchema)