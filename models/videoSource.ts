import { model, Schema, Model, Document, Types } from 'mongoose'

export interface IVideoSource extends Document {
    episode: Types.ObjectId;
    alt_title: string;
    locale: string;
    duration: number;
    video_source: string;
}

const VideoSourceSchema: Schema = new Schema({
    episode: { type: Types.ObjectId, required: true },
    alt_title: { type: String, required: true },
    locale: { type: String, required: true, default: 'en_US' },
    duration: { type: Number, required: true, default: 0 },
    video_source: { type: String, required: true }
}, {
    timestamps: {}
})

// VideoSourceSchema.pre('save', function (next) {
//     next()
// })

export const VideoSource: Model<IVideoSource> = model('VideoSource', VideoSourceSchema)