import { model, Schema, Model, Document, Types } from 'mongoose'

export interface IEpisode extends Document {
    season: Types.ObjectId;
    title: string;
    description: string;
    sort_order: number;
}

const EpisodeSchema: Schema = new Schema({
    season: { type: Types.ObjectId, required: true },
    title: { type: String, required: true },
    description: { type: String },
    sort_order: { type: Number, required: true, default: 0 },
}, {
    timestamps: {}
})

EpisodeSchema.virtual('sources', {
    ref: 'VideoSource',
    localField: '_id',
    foreignField: 'episode'
})

// EpisodeSchema.pre('save', function (next) {
//     next()
// })

export const Episode: Model<IEpisode> = model('Episode', EpisodeSchema)