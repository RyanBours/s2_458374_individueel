import { model, Schema, Model, Document, PassportLocalSchema, PassportLocalModel, PassportLocalDocument } from 'mongoose'
import passportLocalMongoose from 'passport-local-mongoose'

export interface IUser extends PassportLocalDocument {
    email: string;
}

const UserSchema = new Schema({
    email: { type: String, require: true, unique: true },
}, {
    timestamps: {}
}) as PassportLocalSchema

interface UserModel<T extends Document> extends PassportLocalModel<T> { }

UserSchema.plugin(passportLocalMongoose, {
    usernameField: 'email'
})

// UserSchema.pre('save', function (next) {
//     next()
// })

export const User: UserModel<IUser> = model<IUser>('User', UserSchema);