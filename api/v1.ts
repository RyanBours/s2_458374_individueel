import express, { NextFunction, Request, Response } from 'express';

import helmet from 'helmet'
import cors from 'cors'

import logRequest from '../middleware/logRequest'

const v1 = express.Router();

v1.use(logRequest())

// .use(cors())
// .use(helmet())

v1.get(['/test'], (req: Request, res: Response) => {res.send('test')});

export default v1;