import mongoose from 'mongoose'

const mongooseSettings = {
    useCreateIndex: true,
    useNewUrlParser: true,
    dbName: process.env.DB_NAME,
    user: process.env.DB_USER,
    pass: process.env.DB_PASS,
    useUnifiedTopology: true
}

export function init() {
    mongoose.connect(process.env.DB_URL!, mongooseSettings);
    
    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'connection error:'))
    db.once('open', () => console.log('db connected'))
}
    