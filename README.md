# Instructions

clone project:  
`git clone https://RyanBours@bitbucket.org/RyanBours/s2_458374_individueel.git`

Install packages:  
`yarn`

create .env file:  
`touch .env`  
copy the content of **.example.env** to **.env** and edit accordingly

# build resources
`yarn run development`

# development:
`yarn start`

# production:
`yarn run build`

# FAQ
Q: UnhandledPromiseRejectionWarning: Error: Missing delimiting slash between hosts and options

A: missing '/' after port in DB_URL
___

[![forthebadge](https://forthebadge.com/images/badges/works-on-my-machine.svg)](https://forthebadge.com)