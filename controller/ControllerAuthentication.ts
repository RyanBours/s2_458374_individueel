import { Request, Response } from 'express';
import passport from 'passport';
import { User, IUser } from '../models/index'

export function login(req: Request, res: Response): void {
    res.render('auth/login')
}

export function register(req: Request, res: Response): void {
    res.render('auth/register')
}

export function registerPost(req: Request, res: Response): void {
    User.register(new User({ email: req.body.email }), req.body.password, (err: any, user: IUser) => {
        if (err) {
            console.log('error on user registration!', err);
            return res.send(err)// FIXME: error handling duplicate email'
        }
        passport.authenticate('local')(req, res, () => res.redirect('/'))
    })
}

export function logout(req: Request, res: Response): void {
    req.logout();
    res.redirect('/');
}