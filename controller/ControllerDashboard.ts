import { Request, Response } from 'express';

import { Serie, ISerie } from '../models/index'

// index - GET
export async function index(req: Request, res: Response): Promise<void> {
    let series: Array<ISerie> = await Serie.find() 
    res.render('dashboard/index', { title: 'index', series: series})
}