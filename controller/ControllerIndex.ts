import { Request, Response } from 'express';

export function index(req: Request, res: Response): void {
    if (req.user) return res.redirect('/dashboard')
    res.render('index', { title: 'index' })
}