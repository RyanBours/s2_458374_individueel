import { Request, Response } from 'express';

// index - GET
export function index(req: Request, res: Response): void {
    res.render('admin/dashboard/index', { title: 'index' })
}