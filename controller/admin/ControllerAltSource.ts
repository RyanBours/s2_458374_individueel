import { Request, Response } from 'express'
import Busboy from 'busboy'
import { join } from 'path'
import fs from 'fs'
import { v4 as uuidv4 } from 'uuid'

import { Episode, IEpisode, VideoSource, IVideoSource } from '../../models/index'

let destination: string = join(process.env.__root!, 'public/videos')

// GET
export async function create(req: Request, res: Response): Promise<void> {
    let episodes: Array<IEpisode> = await Episode.find()
    return res.render('admin/altsource/create', { episodes: episodes })
}

// POST
export async function post(req: Request, res: Response): Promise<void> {
    let invalidMimeType: boolean = false;
    let busboy = new Busboy({ headers: req.headers })

    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
        if (!mimetype.match('video.*') && file) {
            invalidMimeType = true
            file.resume()
        } else {
            req.body.file = {
                filename: filename,
                stream: file
            }
        }

        file.on('data', (data) => {}) // NOTE: do not remove this, otherwise won't reach finished state
    })

    busboy.on('field', (fieldname, val, fieldnameTruncated, valTruncated) => {
        req.body[fieldname] = val
    })

    busboy.on('finish', async () => {
        // TODO: invalid mimetype flash
        if (invalidMimeType) return res.redirect('back')

        // store file
        let file: any = req.body.file
        let filename = uuidv4()+file.filename
        file.stream.pipe(fs.createWriteStream(join(destination, filename)))

        try {
            if (!await Episode.exists({ _id: req.body.episode_id }))
                throw new Error("Unable to find Episode by id: " + req.body.episode_id)

            let source: IVideoSource = new VideoSource()
                source.episode = req.body.episode_id
                source.alt_title = req.body.alt_title
            if (req.body.locale)
                source.locale = req.body.locale
                // TODO: duration
                source.video_source = filename

            await source.save()
        } catch (error) {
            console.log(error)

            fs.unlink(join(destination, filename), (err) => {
                if (err) {
                    console.error(err)
                    return
                }
            })
            
            // TODO: flash error
            return res.redirect('back')
        }
        return res.redirect('back')
    })

    req.pipe(busboy)
}