import { Request, Response } from 'express';

import { Serie, ISerie, Season, ISeason } from '../../models/index'

// GET
export async function create(req: Request, res: Response): Promise<void> {
    let series: Array<ISerie> = await Serie.find()
    return res.render('admin/season/create', { series: series })
}

// POST
export async function post(req: Request, res: Response): Promise<void> {
    try {

        if (!await Serie.exists({_id: req.body.serie_id}))
            throw new Error("Unable to find Serie by id: " + req.body.serie_id)

        let season: ISeason = new Season()
            season.serie = req.body.serie_id
            season.title = req.body.title
            season.description = req.body.description
        if (req.body.sort_order)
            season.sort_order = req.body.sort_order

        await season.save()

    } catch (error) {
        console.log(error)
        return res.redirect('back') // TODO: flash
    }

    return res.redirect('/admin/serie/show/' + req.body.serie_id)
}