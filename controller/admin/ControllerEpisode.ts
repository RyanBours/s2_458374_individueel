import { Request, Response } from 'express';

import { Season, ISeason, Episode, IEpisode } from '../../models/index'

// GET - TEMP
export async function index(req: Request, res: Response): Promise<void> {
    let episodes: Array<IEpisode> = await Episode.find()
    .populate('episodes')
    res.send(episodes)
}

// GET
export async function create(req: Request, res: Response): Promise<void> {
    let seasons: Array<ISeason> = await Season.find()
    return res.render('admin/episode/create', { seasons: seasons })
}

// POST
export async function post(req: Request, res: Response): Promise<void> {
    try {
        if (!await Season.exists({_id: req.body.season_id}))
            throw new Error("Unable to find Season by id: " + req.body.season_id)

        let episode: IEpisode = new Episode()
            episode.season = req.body.season_id
            episode.title = req.body.title
            episode.description = req.body.description
        if (req.body.sort_order)
            episode.sort_order = req.body.sort_order

        await episode.save()

    } catch (error) {
        return res.redirect('back')
    }

    return res.redirect('back')
}