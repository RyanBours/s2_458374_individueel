import { Request, Response } from 'express';

import { Serie, ISerie } from '../../models/index'

export async function index(req: Request, res: Response): Promise<void> {
    let series: Array<ISerie> = await Serie.find()
    
    res.render('admin/serie/index', {
        title: 'series', series: series
    })
}

// GET
export async function show(req: Request, res: Response): Promise<void> {
    let serie: ISerie | null

    try {
        serie = await Serie
            .findOne({_id: req.params.id})
            .populate({
                path:'seasons', 
                populate: {
                    path: 'episodes',
                    populate: {
                        path: 'sources'
                    }
                }
            })
    } catch (error) {
        console.log(error)
        return res.redirect('back') // TODO: flash 
    }
    res.render('admin/serie/show', { title: 'series TODO', serie: serie })
}

// GET
export function create(req: Request, res: Response): void {
    res.render('admin/serie/create', { title: 'series create' })
}

// POST
export async function post(req: Request, res: Response): Promise<void> {
    let serie: ISerie = new Serie()
    serie.title = req.body.title
    if (req.body.description)
        serie.description = req.body.description
    // TODO: publish
    try {
        await serie.save()
    } catch (error) {
        // TODO: frontent error handling
        // error bag?
        console.log(error)
        return res.redirect('back')
    }
    
    res.redirect('/admin/series')
}

// edit

// put

// delete ?